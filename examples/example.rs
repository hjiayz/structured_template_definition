use structured_template_definition::*;
fn main(){}

pub struct Space;

impl Parser for Space {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<Space, Error> {
        if ctx.document.is_empty() {
            return ctx.result_err(path);
        }
        
        match ctx.document.as_bytes()[0] {
            32|114|116=>{
                ctx.offset(1);
                Ok(Space)
            },
            _ => ctx.result_err(path),
        }

    }
}

impl Builder for Space {
    fn build(&self)->String {
        EMPTYSTR
    }
}


pub struct Eol;

impl Parser for Eol {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<Eol, Error> {
        if ctx.document.is_empty() {
            return ctx.result_err(path);
        }
        
        match ctx.document.as_bytes()[0] {
            110=>{
                ctx.offset(1);
                Ok(Eol)
            },
            _ => ctx.result_err(path),
        }

    }
}

impl Builder for Eol {
    fn build(&self)->String {
        EMPTYSTR
    }
}


pub struct Or;

impl Parser for Or {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<Or, Error> {
        if ctx.document.is_empty() {
            return ctx.result_err(path);
        }
        
        match ctx.document.as_bytes()[0] {
            124=>{
                ctx.offset(1);
                Ok(Or)
            },
            _ => ctx.result_err(path),
        }

    }
}

impl Builder for Or {
    fn build(&self)->String {
        EMPTYSTR
    }
}


pub struct RpcId(u16);

impl Parser for RpcId {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<RpcId, Error> {
        let value = u16::parse(ctx, path)?;
        Ok(RpcId(value))
    }
}

impl Builder for RpcId {
    fn build(&self)->String {
        format!("0x{:X}",self.0)
    }
}

pub struct IdentStart;

impl Parser for IdentStart {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<IdentStart, Error> {
        if ctx.document.is_empty() {
            return ctx.result_err(path);
        }
        
        match ctx.document.as_bytes()[0] {
            97..=122=>{
                ctx.offset(1);
                Ok(IdentStart)
            },
            _ => ctx.result_err(path),
        }

    }
}

impl Builder for IdentStart {
    fn build(&self)->String {
        EMPTYSTR
    }
}


pub struct IdentPart;

impl Parser for IdentPart {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<IdentPart, Error> {
        if ctx.document.is_empty() {
            return ctx.result_err(path);
        }
        
        match ctx.document.as_bytes()[0] {
            48..=57|95|97..=122=>{
                ctx.offset(1);
                Ok(IdentPart)
            },
            _ => ctx.result_err(path),
        }

    }
}

impl Builder for IdentPart {
    fn build(&self)->String {
        EMPTYSTR
    }
}


pub struct Unsigned(u64);

impl Parser for Unsigned {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<Unsigned, Error> {
        let value = u64::parse(ctx, path)?;
        Ok(Unsigned(value))
    }
}

impl Builder for Unsigned {
    fn build(&self)->String {
        format!("0x{:X}",self.0)
    }
}

pub struct Signed(i64);

impl Parser for Signed {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<Signed, Error> {
        let value = i64::parse(ctx, path)?;
        Ok(Signed(value))
    }
}

impl Builder for Signed {
    fn build(&self)->String {
        format!("0x{:X}",self.0)
    }
}

pub struct UnicodeCp(u32);

impl Parser for UnicodeCp {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<UnicodeCp, Error> {
        let value = u32::parse(ctx, path)?;
        match value {
            0 ..= 55295 => Ok(UnicodeCp(value)),
            57344 ..= 1114111 => Ok(UnicodeCp(value)),
            _ => ctx.result_err(path),
        }

    }
}

impl Builder for UnicodeCp {
    fn build(&self)->String {
        format!("0x{:X}",self.0)
    }
}

pub struct CharFirst;

impl Parser for CharFirst {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<CharFirst, Error> {
        if ctx.document.is_empty() {
            return ctx.result_err(path);
        }
        
        match ctx.document.as_bytes()[0] {
            36|91..=93|96|110|116=>ctx.result_err(path),
            _ => {
                ctx.offset(1);
                Ok(CharFirst)
            },
        }

    }
}

impl Builder for CharFirst {
    fn build(&self)->String {
        EMPTYSTR
    }
}


pub struct CharSecond;

impl Parser for CharSecond {
    fn parse(ctx: &mut Context, path: PathNode) -> Result<CharSecond, Error> {
        if ctx.document.is_empty() {
            return ctx.result_err(path);
        }
        
        ctx.offset(ctx.document.chars().next().unwrap().len_utf8());
        Ok(CharSecond)
    }
}

impl Builder for CharSecond {
    fn build(&self)->String {
        EMPTYSTR
    }
}

