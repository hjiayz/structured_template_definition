// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::marker::PhantomData;

pub const EMPTYSTR : String = String::new();

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Error {
    pub range: (usize, usize),
    pub path: Vec<&'static str>,
    pub node: &'static str,
}

#[derive(Clone, Copy,Debug)]
pub struct PathNode<'a> {
    pub parent: Option<&'a PathNode<'a>>,
    pub data: &'static str,
}

impl<'a> PathNode<'a> {
    pub fn new(root: &'static str) -> PathNode<'a> {
        PathNode {
            parent: None,
            data: root,
        }
    }
    pub fn with<'b: 'a>(&'b self, tag: &'static str) -> PathNode<'b> {
        PathNode {
            parent: Some(self),
            data: tag,
        }
    }
    pub fn get_path(&self) -> Vec<&'static str> {
        let mut path = vec![self.data];
        let mut parent = self.parent;
        while let Some(val) = parent {
            path.push(val.data);
            parent = val.parent;
        }
        path.reverse();
        path
    }
}

#[derive(Clone, Copy,Debug)]
pub struct Context<'a> {
    pub pos: usize,
    pub document: &'a str,
}

// https://tools.ietf.org/html/rfc3629
const UTF8_CHAR_WIDTH: &[u8; 256] = &[
    // 1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 1
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 2
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 3
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 4
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 5
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 6
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 7
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 8
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 9
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // A
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // B
    0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // C
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // D
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // E
    4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // F
];

pub const fn utf8_char_width(b: u8) -> usize {
    UTF8_CHAR_WIDTH[b as usize] as usize
}

impl<'a> Context<'a> {
    pub fn offset(&mut self, offset: usize) {
        self.pos += offset;
        self.document = &self.document[offset..];
    }
    pub fn offset_char(&mut self) {
        self.offset(utf8_char_width(self.document.as_bytes()[0]))
    }
    pub fn result_err<T>(&self, path: PathNode<'_>) -> Result<T, Error> {
        Err(self.err(path))
    }
    pub fn err(&self, path: PathNode<'_>) -> Error {
        Error {
            range: (self.pos, self.pos),
            path: path.get_path(),
            node: path.data,
        }
    }
    pub fn err_len(&self, path: PathNode<'_>, len: usize) -> Error {
        Error {
            range: (self.pos, self.pos + len),
            path: path.get_path(),
            node: path.data,
        }
    }
}

pub trait Parser: Sized {
    fn document(document:&str) -> Result<Self, Error>{
        let mut ctx = Context{
            pos:0,
            document,
        };
        let path = PathNode::new("root");
        Self::parse(&mut ctx, path)
    }
    fn check(ctx: &mut Context, path: PathNode) -> Result<usize, Error>{
        let pos = ctx.pos;
        let document = ctx.document;
        let res = Self::parse(ctx, path);
        let len = ctx.pos-pos;
        ctx.pos = pos;
        ctx.document = document;
        res.map(|_|len)
    }
    fn check_offset(ctx: &mut Context, path: PathNode) -> Result<usize, Error>{
        Self::check(ctx,path).map(|len|{
            ctx.offset(len);
            len
        })
    }
    fn parse(ctx: &mut Context, path: PathNode) -> Result<Self, Error>;
    fn try_parse(ctx: &mut Context, path: PathNode) -> Option<Self>{
        let pos = ctx.pos;
        let document = ctx.document;
        match Self::parse(ctx, path).ok() {
            Some(val)=>Some(val),
            None=>{
                ctx.pos = pos;
                ctx.document = document;
                None
            }
        }
    }
}

pub trait Builder {
    fn build(&self)->String;
}

#[derive(Debug)]
pub struct Span<T> {
    pub range: (usize, usize),
    pub node: T,
}

impl<T> Span<T> {
    pub fn map<U, F: Fn(T) -> U>(self, f: F) -> Span<U> {
        Span {
            range: self.range,
            node: f(self.node),
        }
    }
    pub fn text<'a>(&self,document:&'a str)->&'a str{
        &document[self.range.0..self.range.1]
    }
}

impl<T: Parser> Parser for Span<T> {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Span<T>, Error> {
        let start = ctx.pos;
        let node = T::parse(ctx, path)?;
        let end = ctx.pos;
        Ok(Span {
            range: (start, end),
            node,
        })
    }
}

pub fn assert_str(ctx: &mut Context, path: PathNode<'_>, pat: &str) -> Result<(), Error> {
    if ctx.document.starts_with(pat) {
            ctx.offset(pat.len());
        Ok(())
    } else {
        ctx.result_err(path)
    }
}

pub fn assert_strs(ctx: &mut Context, path: PathNode<'_>, pats: &[&str]) -> Result<(), Error> {
    for pat in pats {
        if ctx.document.starts_with(pat) {
                ctx.offset(pat.len());
            return Ok(());
        }
    }
    ctx.result_err(path)
}

pub fn assert_strs_ne(ctx: &mut Context, path: PathNode<'_>, pats: &[&str]) -> Result<(), Error> {
    for pat in pats {
        if ctx.document.starts_with(pat) {
            return ctx.result_err(path);
        }
    }
    ctx.offset_char();
    Ok(())
}

pub fn assert_char(
    ctx: &mut Context,
    path: PathNode<'_>,
    ranges: &[(char, char)],
) -> Result<(), Error> {
    let ch = ctx.document.chars().next().ok_or_else(|| ctx.err(path))?;
    let len = ch.len_utf8();
    for (start, end) in ranges.into_iter() {
        if (*start..=*end).contains(&ch) {
            ctx.offset(len);
            return Ok(());
        }
    }
    ctx.result_err(path)
}

#[test]
fn test_assert_char(){
    let mut ctx = Context {
        pos: 0,
        document: " ".into(),
    };
    if assert_char(&mut ctx, PathNode::new("root"), &[('a','z')]).is_ok() {
        panic!();
    }
}


macro_rules! impl_parse_unsigned {
    ($($ty:ty),*) => ($(
        impl Parser for $ty {
            fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<$ty, Error> {
                let mut len = 0;
                let mut value = 0;
                if ctx.document.starts_with("0x") {
                    len+=2;
                    impl_num_parse!(ctx,len,path,value,shl_maybe_overflow,checked_add,true)
                } else {
                    impl_num_parse!(ctx,len,path,value,mul_maybe_overflow,checked_add,false)
                }
            }
        }
    )*)
}

macro_rules! impl_num_parse_is_hex {
    (true,$ctx:ident,$len:ident,$value:ident) => {
        match $ctx.document.as_bytes()[$len] {
            b @ b'0'..=b'9' => {
                $len = $len + 1;
                b - b'0'
            }
            b @ b'a'..=b'f' => {
                $len = $len + 1;
                b - (b'a' - 10)
            }
            b @ b'A'..=b'F' => {
                $len = $len + 1;
                b - (b'A' - 10)
            }
            _ => {
                    $ctx.offset($len);
                return Ok($value);
            }
        }
    };
    (false,$ctx:ident,$len:ident,$value:ident) => {
        match $ctx.document.as_bytes()[$len] {
            b @ b'0'..=b'9' => {
                $len = $len + 1;
                b - b'0'
            }
            _ => {
                    $ctx.offset($len);
                return Ok($value);
            }
        }
    };
}

trait MaybeOverflow {
    fn shl_maybe_overflow(self) -> Option<Self>
    where
        Self: Sized;
    fn mul_maybe_overflow(self) -> Option<Self>
    where
        Self: Sized;
}

macro_rules! impl_uint {
    ($($ty:ty),*) => ($(
        impl MaybeOverflow for $ty {
            fn shl_maybe_overflow(self) -> Option<Self>
            where
                Self: Sized
            {
                const MAX : $ty = <$ty>::MAX / 16;
                if MAX<self {
                    None
                }
                else {
                    Some(self<<4)
                }
            }
            fn mul_maybe_overflow(self) -> Option<Self>
            where
                Self: Sized
            {
                self.checked_mul(10)
            }
        }
    )*)
}

impl_uint!(u8, u16, u32, u64);

macro_rules! impl_int {
    ($($ty:ty),*) => ($(
        impl MaybeOverflow for $ty {
            fn shl_maybe_overflow(self) -> Option<Self>
            where
                Self: Sized
            {
                const MAX : $ty = <$ty>::MAX /16;
                const MIN : $ty = <$ty>::MIN /16;
                if MAX<self || MIN>self {
                    None
                }
                else {
                    Some(self<<4)
                }
            }
            fn mul_maybe_overflow(self) -> Option<Self>
            where
                Self: Sized
            {
                self.checked_mul(10)
            }
        }
    )*)
}
impl_int!(i8, i16, i32, i64);

macro_rules! impl_num_parse {
    ($ctx:ident,$len:ident,$path:ident,$value:ident,$op1:ident,$op2:ident,$is_hex:ident) => {{
        let prefix = $len;
        loop {
            if $ctx.document[$len..].is_empty() {
                if $len == prefix {
                    return $ctx.result_err($path);
                } else {
                    return Ok($value);
                }
            }
            let byte = impl_num_parse_is_hex!($is_hex, $ctx, $len, $value) as _;

            $value = $value
                .$op1()
                .ok_or_else(|| $ctx.err_len($path, $len))?
                .$op2(byte)
                .ok_or_else(|| $ctx.err_len($path, $len))?;
        }
    }};
}

macro_rules! impl_parse_signed {
    ($($ty:ty),*) => ($(
        impl Parser for $ty {
            fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<$ty, Error> {
                let mut len = 0;
                let mut value = 0;
                let is_positive = match ctx.document.as_bytes()[0] {
                    b'-'=>{
                        len = 1;
                        false
                    },
                    b'+'=>{
                        len = 1;
                        true
                    },
                    _ =>{
                        true
                    }
                };
                let is_hex = ctx.document[len..].starts_with("0x");
                match (is_positive,is_hex){
                    (true,true)=>{
                        len+=2;
                        impl_num_parse!(ctx,len,path,value,shl_maybe_overflow,checked_add,true)
                    }
                    (true,false)=>{
                        impl_num_parse!(ctx,len,path,value,mul_maybe_overflow,checked_add,false)
                    }
                    (false,true)=>{
                        len+=2;
                        impl_num_parse!(ctx,len,path,value,shl_maybe_overflow,checked_sub,true)
                    }
                    (false,false)=>{
                        impl_num_parse!(ctx,len,path,value,mul_maybe_overflow,checked_sub,false)
                    }
                }
            }
        }
    )*)
}

impl_parse_unsigned!(u8, u16, u32, u64);

impl_parse_signed!(i8, i16, i32, i64);

#[test]
fn test_parse_signed() {
    let mut ctx = Context {
        pos: 0,
        document: "0x100".into(),
    };
    assert_eq!(
        u8::parse(&mut ctx, PathNode::new("root")),
        Err(Error {
            range: (0, 5),
            path: vec!["root"],
            node: "root"
        })
    );
    let mut ctx = Context {
        pos: 0,
        document: "0xFF".into(),
    };
    assert_eq!(u8::parse(&mut ctx, PathNode::new("root")), Ok(255));
    let mut ctx = Context {
        pos: 0,
        document: "-0x81".into(),
    };
    assert_eq!(
        i8::parse(&mut ctx, PathNode::new("root")),
        Err(Error {
            range: (0, 5),
            path: vec!["root"],
            node: "root"
        })
    );
    let mut ctx = Context {
        pos: 0,
        document: "-0x80".into(),
    };
    assert_eq!(i8::parse(&mut ctx, PathNode::new("root")), Ok(-128));
    let mut ctx = Context {
        pos: 0,
        document: "0x7F".into(),
    };
    assert_eq!(i8::parse(&mut ctx, PathNode::new("root")), Ok(127));
    let mut ctx = Context {
        pos: 0,
        document: "0x80".into(),
    };
    assert!(i8::parse(&mut ctx, PathNode::new("root")).is_err());
    let mut ctx = Context {
        pos: 0,
        document: "-".into(),
    };
    assert!(i8::parse(&mut ctx, PathNode::new("root")).is_err());
}

pub fn camel_case(ident:&str)->String{
    let mut s = String::new();
    let mut is_first = true;
    for ch in ident.chars(){
        if ch=='_' {
            is_first = true;
            continue;
        }
        if is_first{
            s.push_str(&ch.to_uppercase().to_string());
        }
        else {
            s.push(ch);
        }
        is_first = false;
    }
    s
}


#[derive(Debug)]
pub struct Array<T: Parser, const MIN: u32, const MAX: u32>(pub Vec<T>);

#[derive(Debug)]
pub struct ArraySep<T: Parser, SEP:Parser, const MIN: u32, const MAX: u32,>(pub Vec<T>,pub PhantomData<SEP>);

impl<T: Parser, const MIN: u32, const MAX: u32> Parser for Array<T, MIN, MAX> {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Self, Error> {
        let mut node = vec![];
        for i in 0..=MAX {
            let unit = T::parse(ctx, path);
            if unit.is_err() {
                if i >= MIN {
                    break;
                } else {
                    unit?;
                }
            } else {
                node.push(unit?);
            }
        }
        Ok(Array(node))
    }
}

impl<T: Parser, SEP: Parser, const MIN: u32, const MAX: u32> Parser
    for ArraySep<T, SEP, MIN, MAX>
{
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Self, Error> {
        let mut node = vec![];
        let mut is_first = true;
        for i in 0..=MAX {
            let unit = T::parse(ctx, path);
            if unit.is_err() {
                if (i >= MIN) && is_first {
                    break;
                } else {
                    unit?;
                }
            } else {
                node.push(unit?);
            }
            if SEP::parse(ctx, path).is_err(){
                break;
            }
            is_first=false;
        }
        Ok(ArraySep(node,PhantomData))
    }
}
