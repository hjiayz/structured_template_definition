// Copyright 2022 hjiayz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::fmt::format;
use std::ops::Bound;
use std::ops::RangeBounds;
use std::panic;
use std::vec;

use int_ranges::wrap;
use int_ranges::Range;
use int_ranges::Ranges;

use crate::common::*;
use crate::parser::*;

pub fn build(file: &File) -> String {
    let mut result = String::new();
    for statement in &file.statements {
        result.push_str(&build_stmt(&statement));
    }
    result
}

pub fn build_stmt(stmt: &Statement) -> String {
    match stmt {
        Statement::Type(item) => build_type_stmt(item),
        _ => format!(""),
    }
}

pub fn build_type_stmt(stmt: &TypeStatement) -> String {
    match &stmt.define {
        TypeDefine::Char(item) => build_char_def(&item, &stmt.ident),
        TypeDefine::Int(item) => build_int_def(&item, &stmt.ident),
        TypeDefine::Struct(item) => build_struct_def(&item, &stmt.ident),
        TypeDefine::Template(item) => build_template(&item, &stmt.ident),
        TypeDefine::Uint(item) => build_uint(&item, &stmt.ident),
        TypeDefine::Union(item) => build_union(&item, &stmt.ident),
        TypeDefine::Span(item) => build_span(&item, &stmt.ident),
    }
}

impl CharItem {
    fn get_value(&self) -> u32 {
        if self.0.starts_with("\\") {
            self.0[1..].chars().next().unwrap().into()
        } else {
            self.0.chars().next().unwrap().into()
        }
    }
}

impl CharValueCp {
    fn get_value(&self) -> u32 {
        self.value.get_value()
    }
}

impl CharValue {
    fn get_value(&self) -> u32 {
        match self {
            CharValue::Codepoint(cp) => cp.0,
            CharValue::Literal(lit) => lit.literal.get_value(),
        }
    }
}

impl CharDefine {
    fn list_to_string(&self, ident: &str) -> String {
        let mut ranges = vec![];
        for list_item in &self.list {
            match list_item {
                CharListItem::Once(item) => ranges.push((item.get_value(), item.get_value())),
                CharListItem::Range(item) => {
                    ranges.push((item.start.get_value(), item.end.get_value()))
                }
            };
        }
        let mut ranges = Ranges::from(ranges);
        let mut is_not = self.is_not.is_some();
        if ranges.contains(vec![wrap(0x80..)]) {
            ranges = ranges.invert();
            is_not = !is_not
        }
        if ranges.contains(vec![wrap(0x80..)]) {
            ranges = ranges.intersect(vec![(0, 0xD7FF), (0xE000, 0x10FFFF)]);
            self.unicode(ranges.into(), ident, is_not)
        } else {
            self.ascii(ranges.into(), ident, is_not)
        }
    }
    fn ascii(&self, ranges: Vec<(u32, u32)>, ident: &str, is_not: bool) -> String {
        let arm_ok = ranges
            .iter()
            .map(|(start, end)| {
                if start == end {
                    format!(r#"{start}"#)
                } else {
                    format!(r#"{start}..={end}"#)
                }
            })
            .collect::<Vec<_>>()
            .join("|");
        if arm_ok.is_empty() {
            return if is_not {
                format!("\n        ctx.offset(1);\n        Ok({ident})")
            } else {
                format!("\n        ctx.result_err(path)")
            };
        }
        if is_not {
            format!(
                r#"
        match ctx.document.as_bytes()[0] {{
            {arm_ok}=>ctx.result_err(path),
            _ => {{
                ctx.offset(1);
                Ok({ident})
            }},
        }}
"#
            )
        } else {
            format!(
                r#"
        match ctx.document.as_bytes()[0] {{
            {arm_ok}=>{{
                ctx.offset(1);
                Ok({ident})
            }},
            _ => ctx.result_err(path),
        }}
"#
            )
        }
    }
    fn unicode(&self, ranges: Vec<(u32, u32)>, ident: &str, is_not: bool) -> String {
        let arm_ok = ranges
            .iter()
            .map(|(start, end)| {
                if start == end {
                    format!(r#"'\u{{{:X}}}'"#, start)
                } else {
                    format!(r#"'\u{{{:X}}}'..='\u{{{:X}}}'"#, start, end)
                }
            })
            .collect::<Vec<_>>()
            .join("|");
        if arm_ok.is_empty() {
            return if is_not {
                format!("\n        ctx.offset(ctx.document.chars().next().unwrap().len_utf8());\n        Ok({ident})")
            } else {
                format!("\n        ctx.result_err(path)")
            };
        }
        if is_not {
            format!(
                r#"
        match ctx.document.chars().next().unwrap() {{
            {arm_ok}=>ctx.result_err(path),
            ch => {{
                ctx.offset(ch.len_utf8());
                Ok({ident})
            }},
        }}
"#
            )
        } else {
            format!(
                r#"
        match ctx.document.chars().next().unwrap() {{
            ch @ {arm_ok}=>{{
                ctx.offset(ch.len_utf8());
                Ok({ident})
            }},
            _ => ctx.result_err(path),
        }}
"#
            )
        }
    }
}

pub fn build_char_def(item: &CharDefine, name: &Ident) -> String {
    let ident = camel_case(&name.0.node.0);
    let select = item.list_to_string(&ident);
    format!(
        r#"
pub struct {ident};

impl Parser for {ident} {{
    fn parse(ctx: &mut Context, path: PathNode) -> Result<{ident}, Error> {{
        if ctx.document.is_empty() {{
            return ctx.result_err(path);
        }}
        {select}
    }}
}}

impl Builder for {ident} {{
    fn build(&self)->String {{
        EMPTYSTR
    }}
}}

"#
    )
}

pub fn build_int_def(item: &IntDefine, name: &Ident) -> String {
    let ident = camel_case(&name.0.node.0);
    let ranges: Ranges<i64> = item
        .int_range
        .iter()
        .map(|range| {
            (
                range.start.as_ref().map(|v| v.0).unwrap_or(0),
                range.end.as_ref().map(|v| v.0).unwrap_or(i64::MAX),
            )
        })
        .collect::<Vec<_>>()
        .into();
    let mut ty = "i8";
    let mut ty_range = Ranges::from(vec![(i8::MIN as i64, i8::MAX as i64)]);
    if ranges.contains(Ranges::from(vec![(i8::MIN as i64, i8::MAX as i64)]).invert()) {
        ty = "i16";
        ty_range = Ranges::from(vec![(i16::MIN as i64, i16::MAX as i64)]);
    }
    if ranges.contains(Ranges::from(vec![(i16::MIN as i64, i16::MAX as i64)]).invert()) {
        ty = "i32";
        ty_range = Ranges::from(vec![(i32::MIN as i64, i32::MAX as i64)]);
    }
    if ranges.contains(Ranges::from(vec![(i32::MIN as i64, i32::MAX as i64)]).invert()) {
        ty = "i64";
        ty_range = Ranges::from(vec![(i64::MIN as i64, i64::MAX as i64)]);
    }
    let mut match_list = String::new();
    if ranges!=Ranges::empty() {
        let is_full = ranges == ty_range;
        if is_full {
            match_list.push_str("\n        Ok(");
            match_list.push_str(&ident);
            match_list.push_str("(value))");
        }
        else {
            match_list.push_str("\n        match value {");
            for item in Vec::<(i64,i64)>::from(ranges).into_iter() {
                if item.0==item.1 {
                    match_list.push_str("\n            ");
                    match_list.push_str( &item.0.to_string() );
                    match_list.push_str(" => Ok(");
                    match_list.push_str(&ident);
                    match_list.push_str("(value)),");
                }
                else {
                    match_list.push_str("\n            ");
                    match_list.push_str( &item.0.to_string() );
                    match_list.push_str( " ..= " );
                    match_list.push_str( &item.1.to_string() );
                    match_list.push_str(" => (),");
                }
            }
            match_list.push_str("\n            _ => ctx.result_err(path),");
            match_list.push_str("\n        }\n");
        }
    }
    else {
        match_list.push_str("\n        ctx.result_err(path)");
    }
    format!(
        r#"
pub struct {ident}({ty});

impl Parser for {ident} {{
    fn parse(ctx: &mut Context, path: PathNode) -> Result<{ident}, Error> {{
        let value = {ty}::parse(ctx, path)?;{match_list}
    }}
}}

impl Builder for {ident} {{
    fn build(&self)->String {{
        format!("0x{{:X}}",self.0)
    }}
}}
"#
    )
}

pub fn build_struct_def(item: &StructDefine, name: &Ident) -> String {
    format!(r#"
pub struct {ident}{{{def}}};

impl Parser for {ident} {{
    fn parse(ctx: &mut Context, path: PathNode) -> Result<{ident}, Error> {{
        
    }}
}}

impl Builder for {ident} {{
    fn build(&self)->String {{{def}}}
}}
"#)
}

pub fn build_template(item: &TemplateDefine, name: &Ident) -> String {
    format!(r#""#)
}

pub fn build_uint(item: &UintDefine, name: &Ident) -> String {

    let ident = camel_case(&name.0.node.0);
    let ranges: Ranges<u64> = item
        .uint_range
        .iter()
        .map(|range| {
            (
                range.start.as_ref().map(|v| v.0).unwrap_or(0),
                range.end.as_ref().map(|v| v.0).unwrap_or(u64::MAX),
            )
        })
        .collect::<Vec<_>>()
        .into();
    let mut ty = "u8";
    let mut ty_range = Ranges::from(vec![(u8::MIN as u64, u8::MAX as u64)]);
    if ranges.contains(Ranges::from(vec![(u8::MIN as u64, u8::MAX as u64)]).invert()) {
        ty = "u16";
        ty_range = Ranges::from(vec![(u16::MIN as u64, u16::MAX as u64)]);
    }
    if ranges.contains(Ranges::from(vec![(u16::MIN as u64, u16::MAX as u64)]).invert()) {
        ty = "u32";
        ty_range = Ranges::from(vec![(u32::MIN as u64, u32::MAX as u64)]);
    }
    if ranges.contains(Ranges::from(vec![(u32::MIN as u64, u32::MAX as u64)]).invert()) {
        ty = "u64";
        ty_range = Ranges::from(vec![(u64::MIN as u64, u64::MAX as u64)]);
    }
    let mut match_list = String::new();
    if ranges!=Ranges::empty() {
        let is_full = ranges == ty_range;
        if is_full {
            match_list.push_str("\n        Ok(");
            match_list.push_str(&ident);
            match_list.push_str("(value))");
        }
        else {
            match_list.push_str("\n        match value {");
            for item in Vec::<(u64,u64)>::from(ranges).into_iter() {
                if item.0==item.1 {
                    match_list.push_str("\n            ");
                    match_list.push_str( &item.0.to_string() );
                    match_list.push_str(" => Ok(");
                    match_list.push_str(&ident);
                    match_list.push_str("(value)),");
                }
                else {
                    match_list.push_str("\n            ");
                    match_list.push_str( &item.0.to_string() );
                    match_list.push_str( " ..= " );
                    match_list.push_str( &item.1.to_string() );
                    match_list.push_str(" => Ok(");
                    match_list.push_str(&ident);
                    match_list.push_str("(value)),");
                }
            }
            match_list.push_str("\n            _ => ctx.result_err(path),");
            match_list.push_str("\n        }\n");
        }
    }
    else {
        match_list.push_str("\n        ctx.result_err(path)");
    }
    format!(
        r#"
pub struct {ident}({ty});

impl Parser for {ident} {{
    fn parse(ctx: &mut Context, path: PathNode) -> Result<{ident}, Error> {{
        let value = {ty}::parse(ctx, path)?;{match_list}
    }}
}}

impl Builder for {ident} {{
    fn build(&self)->String {{
        format!("0x{{:X}}",self.0)
    }}
}}
"#
    )
}

pub fn build_union(item: &UnionDefine, name: &Ident) -> String {
    format!(r#""#)
}

pub fn build_span(item: &SpanDefine, name: &Ident) -> String {
    format!(r#""#)
}
