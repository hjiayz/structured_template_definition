// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{io::{Read, Write}, thread::{self, sleep}, time::Duration, process::exit};

use structured_template_definition::parser::parse;
use structured_template_definition::builder::build;

fn main() {    
    //thread::spawn(||{
    //    sleep(Duration::from_secs(20));
    //    exit(1);
    //});
    let mut buf = String::new();
    std::fs::File::open("gammer.std").unwrap().read_to_string(&mut buf).unwrap();
    let file = parse(&buf).unwrap();
    //println!("{:?}",file);
    let code = build(&file);
    println!("{}",code);
    let mut out = std::fs::File::create("examples/example.rs").unwrap();
    out.write_all(b"use structured_template_definition::*;\n").unwrap();
    out.write_all(b"fn main(){}\n").unwrap();
    out.write_all(code.as_bytes()).unwrap();
}
