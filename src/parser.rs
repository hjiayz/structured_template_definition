// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{marker::PhantomData, vec};

use crate::common::*;

#[derive(Debug)]
pub struct File {
    pub statements: Vec<Statement>,
}

#[derive(Debug)]
pub enum Statement {
    Type(TypeStatement),
    EndInfo(EndInfo),
}

#[derive(Debug)]
pub struct TypeStatement {
    pub ident: Ident,
    pub define: TypeDefine,
}

pub fn parse(document: &str) -> Result<File, Error> {
    let mut ctx = Context {
        pos: 0,
        document: document,
    };
    let path = PathNode::new("File");
    let file = File::parse(&mut ctx, path)?;
    if ctx.document.is_empty() {
        Ok(file)
    }
    else {
        ctx.result_err(path)
    }
}

impl Parser for File {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<File, Error> {
        let statements =
            ArraySep::<Statement, EmptyLine, 0, { u32::MAX }>::parse(ctx, path)?.0;
        Array::<EmptyLine, 0, { u32::MAX }>::parse(ctx, path)?;
        Ok(File{
            statements
        })
    }
}

impl Parser for Statement {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<Statement, Error> {
        if let Some(ts) = TypeStatement::try_parse(ctx, path.with("TypeStatement")) {
            return Ok(Statement::Type(ts));
        }
        if let Some(ei) = EndInfo::try_parse(ctx, path.with("EndInfo")) {
            return Ok(Statement::EndInfo(ei));
        }
        ctx.result_err(path)
    }
}

#[derive(Debug)]
pub struct Space;

impl Parser for Space {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Space, Error> {
        assert_strs(ctx, path, &[" ", "\r", "\t"])?;
        Ok(Space)
    }
}

#[derive(Debug)]
pub struct Eol;

impl Parser for Eol {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Eol, Error> {
        assert_str(ctx, path, "\n")?;
        Ok(Eol)
    }
}

#[derive(Debug)]
pub struct Or;
impl Parser for Or {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Or, Error> {
        assert_str(ctx, path, "|")?;
        Ok(Or)
    }
}

#[derive(Debug)]
pub struct EmptyLine;

impl Parser for EmptyLine {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<EmptyLine, Error> {
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        Eol::parse(ctx, path.with("Eol")).map(|_| EmptyLine)
    }
}

#[derive(Debug)]
pub struct EndInfo;

impl Parser for EndInfo {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<EndInfo, Error> {
        Array::<EmptyLine, 0, { u32::MAX }>::parse(ctx, path)?;
        Ok(EndInfo)
    }
}

impl Parser for TypeStatement {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<TypeStatement, Error> {
        Array::<EmptyLine, 0, { u32::MAX }>::parse(ctx, path)?;
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        assert_str(ctx, path, "type")?;
        Array::<Space, 1, { u32::MAX }>::parse(ctx, path)?;
        let ident = Ident::parse(ctx, path)?;
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        assert_str(ctx, path, "=")?;
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        let define = TypeDefine::parse(ctx, path)?;
        Ok(TypeStatement { ident, define })
    }
}

#[derive(Debug)]
pub struct Ident(pub Span<IdentBody>);

impl Parser for Ident {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<Ident, Error> {
        Span::<IdentBody>::parse(ctx, path).map(|val|Ident(val))
    }
}

#[derive(Debug)]
pub struct IdentBody(pub String);

impl Parser for IdentBody {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<IdentBody, Error> {
        let start = ctx.document;
        let mut len = ctx.pos;
        let IdentStart = IdentStart::parse(ctx, path)?;
        let _ = Array::<IdentPart, 0, { u32::MAX }>::parse(ctx, path)?.0;
        len = ctx.pos-len;
        Ok(IdentBody(start[..len].to_owned()))
    }
}

#[derive(Debug)]
pub struct IdentStart;

impl Parser for IdentStart {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<IdentStart, Error> {
        assert_char(ctx, path, &[('a', 'z')])?;
        Ok(IdentStart)
    }
}

#[derive(Debug)]
pub struct IdentPart;

impl Parser for IdentPart {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<IdentPart, Error> {
        assert_char(ctx, path, &[('a', 'z'), ('0', '9'), ('_', '_')])?;
        Ok(IdentPart)
    }
}

#[derive(Debug)]
pub enum TypeDefine {
    Union(UnionDefine),
    Struct(StructDefine),
    Template(TemplateDefine),
    Int(IntDefine),
    Uint(UintDefine),
    Char(CharDefine),
    Span(SpanDefine),
}

impl Parser for TypeDefine {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<TypeDefine, Error> {
        if let Some(union_define) = UnionDefine::try_parse(ctx, path.with("union_define")) {
            return Ok(TypeDefine::Union(union_define));
        }
        if let Some(struct_define) = StructDefine::try_parse(ctx, path.with("struct_define")) {
            return Ok(TypeDefine::Struct(struct_define));
        }
        if let Some(template_define) =
            TemplateDefine::try_parse(ctx, path.with("template_define"))
        {
            return Ok(TypeDefine::Template(template_define));
        }
        if let Some(int_define) = IntDefine::try_parse(ctx, path.with("int_define")) {
            return Ok(TypeDefine::Int(int_define));
        }
        if let Some(uint_define) = UintDefine::try_parse(ctx, path.with("uint_define")) {
            return Ok(TypeDefine::Uint(uint_define));
        }
        if let Some(char_define) = CharDefine::try_parse(ctx, path.with("char_define")) {
            return Ok(TypeDefine::Char(char_define));
        }
        if let Some(span_define) = SpanDefine::try_parse(ctx, path.with("span_define")) {
            return Ok(TypeDefine::Span(span_define));
        }
        ctx.result_err(path)
    }
}
#[derive(Debug)]
pub struct UnionDefine {
    pub items: Vec<Item>,
}

impl Parser for UnionDefine {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<UnionDefine, Error> {
        assert_str(ctx, path, "union")?;
        Array::<Space, 1, { u32::MAX }>::parse(ctx, path)?;
        assert_str(ctx, path, "{")?;
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        Eol::parse(ctx, path)?;
        let items = Array::<Item, 0, { u32::MAX }>::parse(ctx, path)?.0;
        assert_str(ctx, path, "}")?;
        Ok(UnionDefine { items })
    }
}

#[derive(Debug)]
pub struct StructDefine {
    pub items: Vec<Item>,
}

impl Parser for StructDefine {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<StructDefine, Error> {
        assert_str(ctx, path, "struct")?;
        Array::<Space, 1, { u32::MAX }>::parse(ctx, path)?;
        assert_str(ctx, path, "{")?;
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        Eol::parse(ctx, path)?;
        let items = Array::<Item, 0, { u32::MAX }>::parse(ctx, path)?.0;
        assert_str(ctx, path, "}")?;
        Ok(StructDefine { items })
    }
}

#[derive(Debug)]
pub struct Item {
    pub type_name: Ident,
    pub field_name: FieldName,
}

impl Parser for Item {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<Item, Error> {
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        let type_name = Ident::parse(ctx, path)?;
        let field_name = FieldName::parse(ctx, path)?;
        Eol::parse(ctx, path)?;
        Ok(Item {
            type_name,
            field_name,
        })
    }
}

#[derive(Debug)]
pub struct Unsigned(pub u64);

impl Parser for Unsigned {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Unsigned, Error> {
        let value = u64::parse(ctx, path)?;
        Ok(Unsigned(value))
    }
}

#[derive(Debug)]
pub struct Signed(pub i64);

impl Parser for Signed {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Signed, Error> {
        let value = i64::parse(ctx, path)?;
        Ok(Signed(value))
    }
}

#[derive(Debug)]
pub struct TemplateDefine{
    pub templates:Vec<TemplateUnit>,
}

impl Parser for TemplateDefine {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<TemplateDefine, Error> {
        let templates = ArraySep::<TemplateUnit,Or,1,{u32::MAX}>::parse(ctx, path)?.0;
        Ok(TemplateDefine{
            templates
        })
    }
}

#[derive(Debug)]
pub struct TemplateUnit{
    pub items:Vec<TemplateItem>,
}

impl Parser for TemplateUnit {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<TemplateUnit, Error> {
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        assert_str(ctx, path, "`")?;
        let items = Array::<TemplateItem,0,{u32::MAX}>::parse(ctx, path)?.0;
        assert_str(ctx, path, "`")?;
        Array::<Space, 0, { u32::MAX }>::parse(ctx, path)?;
        Ok(TemplateUnit{
            items
        })
    }
}

#[derive(Debug)]
pub enum TemplateItem{
    Template(Template),
    Character(CharItem),
}

impl Parser for TemplateItem {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<TemplateItem, Error> {
        if let Some(template) = Template::try_parse(ctx, path){
            return Ok(TemplateItem::Template(template));
        }
        if let Some(character) = CharItem::try_parse(ctx, path){
            return Ok(TemplateItem::Character(character));
        }
        Err(ctx.err(path))
    }
}

#[derive(Debug)]
pub struct Template{
    pub type_name:TypeIdent,
    pub sep:Option<Ident>,
    pub field_name:Option<FieldName>,
}

impl Parser for Template {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<Template, Error> {
        assert_str(ctx, path, "${")?;
        let type_name=TypeIdent::parse(ctx,path.with("type_ident"))?;
        let sep = Ident::try_parse(ctx, path);
        let field_name = FieldName::try_parse(ctx,path);
        assert_str(ctx, path, "}")?;
        Ok(Template{
            type_name,
            sep,
            field_name
        })

    }
}

#[derive(Debug)]
pub struct TypeIdent{
    pub ident:Ident,
    pub quantifier:Option<Quantifier>,
}

impl Parser for TypeIdent {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<TypeIdent, Error> {
        let ident=Ident::parse(ctx,path.with("ident"))?;
        let quantifier = Quantifier::try_parse(ctx, path);
        Ok(TypeIdent{
            ident,
            quantifier
        })

    }
}

#[derive(Debug)]
pub struct Quantifier{
    pub range:UintRange,
}

impl Parser for Quantifier {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Quantifier, Error> {
        assert_str(ctx, path, "[")?;
        let range = UintRange::parse(ctx, path)?;
        assert_str(ctx, path, "]")?;
        Ok(Quantifier{
            range
        })
    }
}

#[derive(Debug)]
pub struct UintRange{
    pub start:Option<Unsigned>,
    pub end:Option<Unsigned>
}

impl Parser for UintRange {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<UintRange, Error> {
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        let start = Unsigned::try_parse(ctx,path);
        assert_str(ctx, path, "..")?;
        let end = Unsigned::try_parse(ctx, path);
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        Ok(UintRange{
            start,
            end,
        })
    }
}

#[derive(Debug)]
pub struct IntRange{
    pub start:Option<Signed>,
    pub end:Option<Signed>,
}

impl Parser for IntRange {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<IntRange, Error> {
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        let start = Signed::try_parse(ctx,path);
        assert_str(ctx, path, "..")?;
        let end = Signed::try_parse(ctx, path);
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        Ok(IntRange{
            start,
            end,
        })
    }
}


#[derive(Debug)]
pub struct FieldName{
    pub field_name:Ident,
    pub id:Unsigned,
}

impl Parser for FieldName {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<FieldName, Error> {
        Array::<Space,1,{u32::MAX}>::parse(ctx, path)?;
        let field_name = Ident::parse(ctx,path)?;
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        assert_str(ctx, path, "=")?;
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        let id = Unsigned::parse(ctx, path)?;
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        Ok(FieldName{
            field_name,
            id,
        })
    }
}

#[derive(Debug)]
pub struct CharItem(pub String);

impl Parser for CharItem {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharItem, Error> {
        let start = ctx.document;
        let mut len = ctx.pos;
        if CharFirst::parse(ctx,path).is_ok() {
            len = ctx.pos-len;
            return Ok(CharItem(start[0..len].to_owned()))
        }
        if CharSecond::parse(ctx,path).is_ok() {
            len = ctx.pos-len;
            return Ok(CharItem(start[0..len].to_owned()))
        }
        ctx.result_err(path)
    }
}

#[derive(Debug)]
pub struct CharFirst;

impl Parser for CharFirst{
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<Self, Error> {
        assert_strs_ne(ctx, path, &["$" , "\\" , "\t" , "\n" , "[" , "]" , "`"]).map(|_|CharFirst)
    }
}

#[derive(Debug)]
pub struct CharSecond;

impl Parser for CharSecond {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharSecond, Error> {
        assert_str(ctx, path, "\\")?;
        assert_char(ctx, path, &[('\u{E000}','\u{10FFFF}'),('\u{0}','\u{D7FF}')]).map(|_|CharSecond)
    }
}

#[derive(Debug)]
pub struct IntDefine{
    pub int_range:Vec<IntRange>,
}

impl Parser for IntDefine {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<IntDefine, Error> {
        assert_str(ctx, path, "int")?;
        let int_range = Array::<IntRange,0,{u32::MAX}>::parse(ctx, path)?.0;
        Ok(IntDefine{
            int_range,
        })
    }
}

#[derive(Debug)]
pub struct UintDefine{
    pub uint_range:Vec<UintRange>,
}

impl Parser for UintDefine {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<UintDefine, Error> {
        assert_str(ctx, path, "uint")?;
        let uint_range = ArraySep::<UintRange,Or,0,{u32::MAX}>::parse(ctx, path)?.0;
        Ok(UintDefine{
            uint_range,
        })
    }
}

#[derive(Debug)]
pub struct CharValueCp{
    pub value:CharValue,
}

impl Parser for CharValueCp {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharValueCp, Error> {
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        let value = CharValue::parse(ctx, path)?;
        Array::<Space,0,{u32::MAX}>::parse(ctx, path)?;
        Ok(CharValueCp{
            value
        })
    }
}

#[derive(Debug)]
pub struct CharDefine{
    pub is_not:Option<CharIsNot>,
    pub list:Vec<CharListItem>,
}

impl Parser for CharDefine {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharDefine, Error> {
        assert_str(ctx, path, "char")?;
        Array::<Space,1,{u32::MAX}>::parse(ctx, path)?;
        let is_not = CharIsNot::try_parse(ctx,path);
        let list = ArraySep::<CharListItem,Or,1,{u32::MAX}>::parse(ctx, path)?.0;
        Ok(CharDefine{
            is_not,
            list,
        })
    }
}

#[derive(Debug)]
pub struct CharIsNot;

impl Parser for CharIsNot {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharIsNot, Error> {
        assert_str(ctx, path, "!").map(|_|CharIsNot)
    }
}

#[derive(Debug)]
pub enum CharListItem{
    Range(CharRange),
    Once(CharValueCp),
}

impl Parser for CharListItem {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharListItem, Error> {
        if let Some(range) = CharRange::try_parse(ctx, path) {
            return Ok(CharListItem::Range(range));
        }
        CharValueCp::parse(ctx, path).map(|val|CharListItem::Once(val))
    }
}

#[derive(Debug)]
pub struct CharRange{
    pub start:CharValueCp,
    pub end:CharValueCp,
}

impl Parser for CharRange {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharRange, Error> {
        let start = CharValueCp::parse(ctx,path)?;
        assert_str(ctx, path, "..")?;
        let end = CharValueCp::parse(ctx,path)?;
        Ok(CharRange{
            start,
            end,
        })
    }
}

#[derive(Debug)]
pub enum CharValue{
    Literal(CharLiteral),
    Codepoint(UnicodeCp),
}

impl Parser for CharValue {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharValue, Error> {
        if let Some(literal) = CharLiteral::try_parse(ctx, path) {
            return Ok(CharValue::Literal(literal));
        }
        UnicodeCp::parse(ctx, path).map(|val|CharValue::Codepoint(val))
    }
}

#[derive(Debug)]
pub struct CharLiteral{
    pub literal:CharItem,
}

impl Parser for CharLiteral {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<CharLiteral, Error> {
        assert_str(ctx, path, "`")?;
        let literal = CharItem::parse(ctx, path)?;
        assert_str(ctx, path, "`")?;
        Ok(CharLiteral{
            literal
        })
    }
}

#[derive(Debug)]
pub struct UnicodeCp(pub u32);

impl Parser for UnicodeCp {
    fn parse(ctx: &mut Context, path: PathNode<'_>) -> Result<UnicodeCp, Error> {
        let origin = ctx.to_owned();
        let value = u32::parse(ctx, path)?;
        if (0xE000..=0x10FFFF).contains(&value) || (0x0..=0xD7FF).contains(&value) {
            return Ok(UnicodeCp(value));
        }
        ctx.document=origin.document;
        ctx.pos=origin.pos;
        ctx.result_err(path)
    }
}

#[derive(Debug)]
pub struct SpanDefine{
    pub type_ident:TypeIdent,
}

impl Parser for SpanDefine {
    fn parse(ctx: &mut Context<'_>, path: PathNode<'_>) -> Result<SpanDefine, Error> {
        assert_str(ctx, path, "span")?;
        Array::<Space,1,{u32::MAX}>::parse(ctx, path)?;
        let type_ident = TypeIdent::parse(ctx, path)?;
        Ok(SpanDefine{
            type_ident,
        })
    }
}